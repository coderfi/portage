coderfi Overlay
===============

Gentoo overlay of custom ebuilds.

Using this overlay through layman:
----------------------------------

- "emerge layman"

- Edit the layman configuration "vi /etc/layman/layman.cfg" and add 
  https://coderfi@bitbucket.org/coderfi/portage.git
  to the 'overlays:' section.

- Now run "layman -a coderfi" to add the overlay

- if it's your first run of layman you have to create the cache first using 'layman -L'

OR alternativlely

- For layman-2.0.0_rc* and later

- Simply download the coderfi-overlay.xml to /etc/layman/overlays/coderfi-overlay.xml

- run "layman -a coderfi" to add the overlay

Credits
-------

The [OpenStack Overlay Git](https://github.com/hyves-org/openstack-overlay) for the helpful README file.

