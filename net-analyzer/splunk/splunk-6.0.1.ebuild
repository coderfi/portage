# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
# customized from https://github.com/brutester/gentoo/blob/master/custom-net/splunk/splunk-5.0.2.ebuild

EAPI=5

MY_BUILD="189883"
DESCRIPTION="The search engine for IT data"
HOMEPAGE="http://www.splunk.com"
SRC_URI="${REL_URI}/splunk/linux/${P}-${MY_BUILD}-Linux-x86_64.tgz"
RESTRICT="fetch"

LICENSE="splunk-eula"
SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

#override source dir inside the tarball
S="${PORTAGE_BUILDDIR}/work/splunk"

pkg_nofetch() {
    einfo "Please download"
    einfo "  ${P}-${MY_BUILD}-Linux-x86_64.tgz"
    einfo "from ${HOMEPAGE} and place it in ${DISTDIR}"    
}

src_install() {
        insinto /opt/${PN}
        doins -r . || die "Install failed!"

        # Install init script
        doinitd "${FILESDIR}"/splunkd || die "doinitd failed"

        # Adjust permissions on executables
        cd "${D}/opt/${PN}/bin"
        for b in `ls .`; do
                fperms 755 "${b}" || die "fperms failed on ${b}"
        done
}

pkg_postinst() {
        einfo "Creating default configuration to monitor /var/log"
        # Need to start splunk to accept the license and build database
        /opt/${PN}/bin/splunk start --accept-license
        /opt/${PN}/bin/splunk stop

        elog "A default configuration has been created to monitor /var/log."
        elog ""
		elog "See /opt/${PN}/etc"
		elog ""
        elog "For more information about Splunk, please visit"
        elog "${HOMEPAGE}/doc/latest"
        elog ""
        elog "To add splunk to your startup scripts"
        elog "run 'rc-update add splunkd default'"
}

